import React, {Component} from "react";
import ReactDOM from "react-dom";
import Login from "./components/Login";
import Register from "./components/Register";
import ChangePassword from "./components/ChangePassword";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/css/bootstrap-theme.min.css";
import "../assets/css/app.css";
import {BrowserRouter, Route} from 'react-router-dom'
import {Link} from 'react-router-dom'
import {render} from 'react-dom';
// Import routing components
import {Router, Switch, browserHistory} from 'react-router';


class Home extends Component {
  render() {
    return (<div><h1>Xebia</h1><Link to="/login">Login</Link> ----
      <Link to="/register">Register</Link> ----
      <Link to="/password">Forgot Password</Link></div>);
  }
}

ReactDOM.render((
    <BrowserRouter>
      <Switch>
        <Route path="/login" component={Login}/>
        <Route path="/register" component={Register}/>
        <Route path="/password" component={ChangePassword}/>
        <Route path="/" component={Home}/>
      </Switch>
    </BrowserRouter>
  ),
  document.getElementById('root')
);




