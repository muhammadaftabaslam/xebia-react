import React, {Component} from "react";
import {Link} from 'react-router-dom';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      mobile: "",
      email: "",
      checkboxForMobileNumber: true,

      account: "",
      checkboxForAccountNumber: false,

      username: "",
      password: "",
      confirmPassword: "",
      newsletter: false,
      marketing: false,


      errors: {}
    };
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleMobileChange = this.handleMobileChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleMobileUserNameChange = this.handleMobileUserNameChange.bind(this);

    this.handleAccountNumberChange = this.handleAccountNumberChange.bind(this);
    this.handleAccountNumberUserNameChange = this.handleAccountNumberUserNameChange.bind(this);

    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleConfirmPasswordChange = this.handleConfirmPasswordChange.bind(this);

    this.handleNewsLetterChange = this.handleNewsLetterChange.bind(this);
    this.handleMarketingChange = this.handleMarketingChange.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getData = this.getData.bind(this);
  }

  handleFirstNameChange(e) {
    this.setState({firstName: e.target.value});
  }

  handleLastNameChange(e) {
    this.setState({lastName: e.target.value});
  }

  handleEmailChange(e) {
    this.setState({email: e.target.value});
  }

  handleMobileChange(e) {
    this.setState({mobile: e.target.value});
  }

  handleMobileUserNameChange(e) {
    this.setState({checkboxForMobileNumber: e.target.value});
  }

  handleAccountNumberChange(e) {
    this.setState({account: e.target.value});
  }

  handleAccountNumberUserNameChange(e) {
    this.setState({checkboxForAccountNumber: e.target.value});
  }

  handleUserNameChange(e) {
    this.setState({username: e.target.value});
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value});
  }

  handleConfirmPasswordChange(e) {
    this.setState({confirmPassword: e.target.value});
  }

  handleNewsLetterChange(e) {
    this.setState({newsletter: !this.state.newsletter});
  }

  handleMarketingChange(e) {
    this.setState({marketing: !this.state.marketing});
  }

  validateFields() {
    const nameReg = /^[A-Za-z]+$/,
      emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let valid = true,
      errors = {};
    // validate first name
    if (!nameReg.test(this.state.firstName)) {
      errors.firstName = "First name should contain only small and capital letters, no numbers, special characters, etc.";
      valid = false;
    }

    // validate last name
    if (!nameReg.test(this.state.lastName)) {
      errors.lastName = "Last name should contain only small and capital letters, no numbers, special characters, etc.";
      valid = false;
    }

    // validate preferred mobile number
    if (!emailReg.test(this.state.mobile)) {
      errors.mobile = "Mobile Number - must be a valid number.";
      valid = false;
    }

    // validate email
    if (!emailReg.test(this.state.email)) {
      errors.email = "email - must be a valid email address";
      valid = false;
    }

    // validate account
    if (!emailReg.test(this.state.account)) {
      errors.account = "account - must be a valid email address";
      valid = false;
    }

    //username
    if (!emailReg.test(this.state.username)) {
      errors.username = "username - must be a valid email address";
      valid = false;
    }

    if (!emailReg.test(this.state.password)) {
      errors.password = "ePassword - must be a valid email address";
      valid = false;
    }

    if (!emailReg.test(this.state.confirmPassword)) {
      errors.confirmpassword = "confirmPassword - must be a valid email address";
      valid = false;
    }

    this.setState({errors: errors});
    return valid;
  }

  getData() {
    return {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      mobile: this.state.mobile,
      email: this.state.email,
      checkboxForMobileNumber: this.state.checkboxForMobileNumber,

      account: this.state.account,
      checkboxForAccountNumber: this.state.checkboxForAccountNumber,

      username: this.state.username,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      newsletter: this.state.newsletter,
      marketing: this.state.marketing
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    let valid = this.validateFields();
    let data = this.getData();
    debugger
    valid && alert(JSON.stringify(data));

    if (valid) {
      fetch('https://reqres.in/api/register', {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.getData())
      }).then((response) => {
        if (response.status == 200) {
          alert("You have successfully registered. Need to work on next step.");
        }
        else {
          let errors = {};
          errors.response = "Your Username or password is incorrect. Try again.";
          this.setState({errors: errors});
        }
      });
    }


  }

  render() {
    return (
      <div>
        <div className={['profile-settings-pattern', 'manage-pages-pattern', 'create-profile'].join(' ')}>
          <div className={['container-fluid'].join(' ')}>
            <div className={['row', 'main-text', 'main-text-A', 'main-text-profile-settings'].join(' ')}>
              <div className={['col-xs-10', 'col-xs-offset-1', 'col-md-8', 'col-md-offset-2'].join(' ')}>
                <h2>Create Profile</h2>
              </div>
            </div>
          </div>
        </div>
        <div className={['bg-gray'].join(' ')}>
          <div className={['help-me', 'container-fluid', 'action-bar-container-active'].join(' ')}>
            <div className={['create-profile-forms', 'forms-default-settings'].join(' ')}>
              <div className={['consumer-register-form', 'profile-setting-col-1', 'container-fluid'].join(' ')}>
                <div className={['row form-container'].join(' ')}>
                  <div
                    className={['col-xs-10', 'col-xs-offset-1', 'col-xs-offset-right-1', 'col-sm-10', 'col-sm-offset-1', 'col-sm-offset-right-1', 'col-md-8', 'col-md-offset-2', 'col-md-offset-right-2'].join(' ')}>
                    <div className={['form-title'].join(' ')}>
                      <h3 className={['mb0'].join(' ')}>Personal Information</h3>
                    </div>
                    <form onSubmit={this.handleSubmit} className={['form', 'contact-form', 'modified-form'].join(' ')}
                          data-active="true"
                          noValidate="false">

                      <div className={['row'].join(' ')}>
                        <div className={[' form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-input'].join(' ')}>
                            <input name="firstName" onChange={this.handleFirstNameChange} required type="text"/>
                            <label>
                              First Name*
                            </label>
                          </div>
                          <span className="error">{this.state.errors.firstName}</span>
                        </div>

                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-input'].join(' ')}>
                            <input name="lastName" onChange={this.handleLastNameChange} required type="text"/>
                            <label>
                              Last Name*
                            </label>
                            <span className="error">{this.state.errors.lastName}</span>
                          </div>
                        </div>
                      </div>

                      {/*<div className={['row'].join(' ')}>
                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-select', 'compound-control'].join(' ')}>
                            <select name="documentTypeSelect" required>
                              <option value="" defaultValue="1">
                              </option>
                              <option value="feedback">Type 1</option>
                              <option value="support">Type 1</option>
                            </select>
                            <label>
                              Document Type*
                            </label>
                          </div>
                        </div>

                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-input'].join(' ')}>
                            <input name="documentType" required type="text"/>
                            <label>
                              Document Type*
                            </label>
                          </div>
                        </div>F
                      </div>*/}

                      <div className="row">
                        {/*<div className={['form-section', 'col-xs-12', 'col-sm-6'].join(' ')} data-personalinfo="true">
                          <div
                            className={['form-group', 'floating-label-input', 'floating-label-datepicker', 'compound-control'].join(' ')}
                            data-personalinfo="true">
                            <input name="birthDate" type="text" className={['datepicker'].join(' ')} required=""/>
                            <label>Date of Birth*</label>
                            <span className={['inner-icon', 'calendar_new-img'].join(' ')}></span>

                          </div>
                        </div>*/}
                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-input'].join(' ')}>
                            <input name="mobileNumber" onChange={this.handleMobileChange} required="" type="text"/>
                            <label>
                              Preferred Mobile Number*
                            </label>
                            <span className="error">{this.state.errors.mobile}</span>
                          </div>
                        </div>
                        <div className={['row'].join(' ')}>
                          <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                            <div className={['form-group', 'floating-label-input'].join(' ')}>
                              <input name="emailAddress" onChange={this.handleEmailChange} required type="text"/>
                              <label>
                                Email Address*
                              </label>
                              <span className="error">{this.state.errors.email}</span>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div className={['row'].join(' ')}>
                        <div
                          className={['horizontal-actions', 'horizontal-checkboxes', 'form-group', 'form-section', 'text-left', 'col-xs-12', 'col-sm-6', 'col-md-6 pr0'].join(' ')}>
                          <div className={['radio', 'radio-inline'].join(' ')}>
                            <input type="radio" name="setAsPrimary" value="abc"
                                   checked={this.state.checkboxForMobileNumber}
                                   onChange={this.handleMobileUserNameChange}
                                   className={['copyAccountNumber'].join(' ')}/>
                            <label>Use as my username </label>
                          </div>
                        </div>
                      </div>

                      <div className={['form-title'].join(' ')}>
                        <h3 className={['mb0'].join(' ')}>Account Information</h3>
                      </div>

                      <div className={['row'].join(' ')}>
                        {/*<div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-select', 'compound-control'].join(' ')}>
                            <select value={this.state.value} onChange={this.handleChange}>
                              <option value=""></option>
                              <option value="feedback">Type 1</option>
                              <option value="support">Type 2</option>
                            </select>
                            <label>
                              Account Type*
                            </label>
                          </div>
                        </div>*/}

                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-input'].join(' ')}>
                            <input name="accountNumber" onChange={this.handleAccountNumberChange} required type="text"/>
                            <label>
                              Account Number*
                            </label>
                            <span className="error">{this.state.errors.account}</span>
                          </div>
                        </div>

                      </div>
                      <div className={['row'].join(' ')}>
                        <div
                          className={['horizontal-actions', 'horizontal-checkboxes', 'form-group', 'form-section', 'text-left', 'col-xs-12', 'col-sm-6', 'col-md-6', 'pr0'].join(' ')}>
                          <div className={['radio', 'radio-inline'].join(' ')}>
                            <input type="radio" name="setAsPrimary" checked={this.state.checkboxForAccountNumber}
                                   onChange={this.handleAccountNumberUserNameChange}
                                   className={['copyEmail'].join(' ')}/>
                            <label>Use as my username </label>
                          </div>
                        </div>
                      </div>


                      <div className={['form-title'].join(' ')}>
                        <h3 className={['mb0'].join(' ')}>Login Credentials</h3>
                      </div>

                      <div className={['row'].join(' ')}>
                        <div className={[' form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['form-group', 'floating-label-input'].join(' ')}>
                            <input name="username" required onChange={this.handleUserNameChange} type="text"/>
                            <label>
                              Username*
                            </label>
                            <span className="error">{this.state.errors.username}</span>
                          </div>
                        </div>
                      </div>
                      <div className={['row'].join(' ')}>
                        <div className={[' form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div
                            className={['form-group', 'floating-label-input', 'compound-control', 'pass-no-icon'].join(' ')}>
                            <input name="passwordUser" id="def-password" type="password"
                                   onChange={this.handlePasswordChange} required/>
                            <label>
                              Password*
                            </label>
                            <span className={['icon-view-password'].join(' ')}></span>
                            <span className={['icon-hide-view-password hide'].join(' ')}></span>
                            <span className="error">{this.state.errors.password}</span>
                          </div>
                        </div>
                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div
                            className={['form-group', 'floating-label-input', 'compound-control', 'pass-no-icon'].join(' ')}>
                            <input name="confirmPasswordUser" id="confirmDef-password" type="password" required
                                   onChange={this.handleConfirmPasswordChange}/>
                            <label>
                              Confirm Password*
                            </label>
                            <span className={['icon-view-confirm-password'].join(' ')}></span>
                            <span className={['icon-hide-view-confirm-password', 'hide'].join(' ')}></span>
                            <span className="error">{this.state.errors.confirmpassword}</span>
                          </div>
                        </div>
                      </div>

                      <div className={['row'].join(' ')}>
                        <div className={[' col-xs-12', 'col-sm-6 password-note'].join(' ')}>
                          <div className={['pwd-meter-track'].join(' ')}>
                            <div id="pwd-meter"></div>
                          </div>
                          <span>NOTE:  Your password must be atleast 6 characters & alphanumeric.</span>
                        </div>
                      </div>

                      <div className={['form-title'].join(' ')}>
                        <h3 className={['mb0'].join(' ')}>Newsletters</h3>
                      </div>

                      <div className={['row', 'newsletters-wrapper'].join(' ')}>
                        <div className={['form-section col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['box-newsletters'].join(' ')}>
                            <div className={['row'].join(' ')}>
                              <div className={['news-letter-content col-xs-7', 'col-sm-8'].join(' ')}>
                                <h4>Smiles newsletter</h4>
                                <p>Subscribe and accept <a href="#">Terms & Conditions</a></p>
                              </div>
                              <div className={['news-letter-switch', 'col-xs-5', 'col-sm-4'].join(' ')}>
                                <div className={['material-switch'].join(' ')}>
                                  <input id="SmilesSwitch" name="SmilesSwitch" type="checkbox"
                                         checked={this.state.newsletter}/>
                                  <label className={['label-success'].join(' ')}
                                         onClick={this.handleNewsLetterChange}></label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className={[' form-section col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div className={['box-newsletters'].join(' ')}>
                            <div className={['row'].join(' ')}>
                              <div className={['news-letter-content', 'col-xs-7', 'col-sm-8'].join(' ')}>
                                <h4>Marketing options</h4>
                                <p>Subscribe and accept <a href="#">Terms & Conditions</a></p>
                              </div>
                              <div className={['news-letter-switch', 'col-xs-5', 'col-sm-4'].join(' ')}>
                                <div className={['material-switch'].join(' ')}>
                                  <input id="WeeklySwitch" name="WeeklySwitch" type="checkbox"
                                         checked={this.state.marketing}/>
                                  <label className={['label-success'].join(' ')}
                                         onClick={this.handleMarketingChange}></label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className={['row'].join(' ')}>
                        <div className={['form-section', 'col-xs-12'].join(' ')}>
                          <div className={['col-sm-6', 'col-sm-offset-3', 'visualCaptcha'].join(' ')}></div>
                        </div>
                      </div>
                      <div className={['form-submit', 'btn-default-margins'].join(' ')}>
                        <button className={['btn', 'btn-default', 'active', 'ripple-effect', 'btn-big'].join(' ')}
                                type="submit">
                          Submit
                        </button>
                        <span className="error">{this.state.errors.response}</span>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
