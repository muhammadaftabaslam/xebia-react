import React, { Component } from "react";
import PropTypes from "prop-types";

class InputField extends Component {
  render() {
    const { name, controlFunc, label, errorMsg } = this.props;
    return (
      <div className="input-field">
        <label>{label}</label>
        <input
          name={name}
          type="text"
          onBlur={(e) => controlFunc(e)}
          />
        <span className="error">{errorMsg}</span>
      </div>
    );
  }
}

InputField.propTypes = {
  controlFunc: PropTypes.func,
  errorMsg: PropTypes.string,
  label: PropTypes.string,
  name: PropTypes.string
};

export default InputField;
