import {Link} from 'react-router-dom'


import React, {Component} from "react";

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      remember: false,
      errors: {}
    };
    /*this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleRememberCheckbox = this.handleRememberCheckbox.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getData = this.getData.bind(this);*/
  }

  /*handleRememberCheckbox(e) {
    this.setState({remember: !this.state.remember});
  }

  handleEmailChange(e) {
    this.setState({email: e.target.value});
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value});
  }

  validateFields() {
    const passReg = /^[0-9a-zA-Z]{8,}$/,
      emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let valid = true,
      errors = {};

    // validate email
    if (!emailReg.test(this.state.email)) {
      errors.email = "email - must be a valid email address";
      valid = false;
    }

    if (!passReg.test(this.state.password)) {
      errors.password = "Password is not correct";
      valid = false;
    }
    this.setState({errors: errors});
    return valid;
  }

  getData() {
    return {
      email: this.state.email,
      password: this.state.password,
      remember: this.state.remember
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    let one = e.target.checkValidity();
    let valid = this.validateFields();
    //valid = this.bankAccounts.validateFields() && valid;  // break shortcircuit and always call `bankAccounts.validateFields`
    if (one && valid) {//&& alert(JSON.stringify(this.getData()));
      fetch('https://reqres.in/api/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, *!/!*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.getData())
      }).then((response) => {
        if (response.status == 200) {
          alert("You have successfully logged in. Need to work on next step.");
        }
        else {
          let errors = {};
          errors.response = "Your Username or password is incorrect. Try again.";
          this.setState({errors: errors});
        }
      });
    }
  }*/

  render() {
    return (
      <div>
        <div className={['profile-settings-pattern', 'manage-pages-pattern', 'create-profile'].join(' ')}>
          <div className={['container-fluid'].join(' ')}>
            <div className={['row', 'main-text', 'main-text-A', 'main-text-profile-settings'].join(' ')}>
              <div className={['col-xs-10', 'col-xs-offset-1', 'col-md-8', 'col-md-offset-2'].join(' ')}>
                <h2>Reset your password</h2>
              </div>
            </div>
          </div>
        </div>
        <div className={['bg-gray'].join(' ')} class="">
          <div className={['help-me', 'container-fluid', 'action-bar-container-active'].join(' ')}>
            <div className={['create-profile-forms', 'forms-default-settings'].join(' ')}>
              <div className={['consumer-register-form', 'profile-setting-col-1', 'container-fluid'].join(' ')}>
                <div className={['row', 'form-container'].join(' ')}>
                  <div
                    className={['col-xs-10', 'col-xs-offset-1', 'col-xs-offset-right-1', 'col-sm-10', 'col-sm-offset-1', 'col-sm-offset-right-1', 'col-md-8', 'col-md-offset-2', 'col-md-offset-right-2'].join(' ')}>
                    <div className={['form-title'].join(' ')}>
                      <h3 className={['mb0'].join(' ')}>Personal Information</h3>
                    </div>
                    <form className={['form', 'contact-form', 'modified-form'].join(' ')} noValidate="false">
                      <div className={['row'].join(' ')}>
                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div
                            className={['form-group', 'floating-label-input', 'compound-control', 'pass-no-icon'].join(' ')}>
                            <input name="passwordUser" id="def-password" type="password" required/>
                            <label>
                              Password*
                            </label>
                            <span className={['icon-view-password'].join(' ')}></span>
                            <span className={['icon-hide-view-password', 'hide'].join(' ')}></span>
                          </div>
                          <div className={['pwd-meter-track'].join(' ')}>
                            <div id="pwd-meter"></div>
                          </div>
                        </div>
                        <div className={['form-section', 'col-xs-12', 'col-sm-6', 'col-md-6'].join(' ')}>
                          <div
                            className={['form-group', 'floating-label-input', 'compound-control', 'pass-no-icon'].join(' ')}>
                            <input name="confirmPasswordUser" id="confirmDef-password" type="password" required/>
                            <label>
                              Confirm Password*
                            </label>
                            <span className={['icon-view-confirm-password'].join(' ')}>
                  </span>
                            <span className={['icon-hide-view-confirm-password', 'hide'].join(' ')}></span>
                          </div>
                        </div>
                      </div>
                      <div className={['row'].join(' ')}>
                        <div className={['col-xs-12', 'col-sm-6', 'password-note'].join(' ')}>
                          <span>NOTE:  Your password must be atleast 6 characters & alphanumeric.</span>
                        </div>
                      </div>
                      <div className={['row'].join(' ')}>
                        <div className={['form-section', 'col-xs-12'].join(' ')}>
                          <div className={['col-sm-6', 'col-sm-offset-3', 'visualCaptcha'].join(' ')}></div>
                        </div>
                      </div>
                      <div className={['form-submit', 'btn-default-margins'].join(' ')}>
                        <button className={['btn', 'btn-default', 'active', 'ripple-effect', 'btn-big'].join(' ')}
                                type="submit">
                          reset password
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChangePassword;
