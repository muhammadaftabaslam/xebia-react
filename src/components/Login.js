import {Link} from 'react-router-dom'


import React, {Component} from "react";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      remember: false,
      errors: {}
    };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleRememberCheckbox = this.handleRememberCheckbox.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.getData = this.getData.bind(this);
  }

  handleRememberCheckbox(e) {
    this.setState({remember: !this.state.remember});
  }

  handleEmailChange(e) {
    this.setState({email: e.target.value});
  }

  handlePasswordChange(e) {
    this.setState({password: e.target.value});
  }

  validateFields() {
    const passReg = /^[0-9a-zA-Z]{8,}$/,
      emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let valid = true,
      errors = {};

    // validate email
    if (!emailReg.test(this.state.email)) {
      errors.email = "email - must be a valid email address";
      valid = false;
    }

    if (!passReg.test(this.state.password)) {
      errors.password = "Password is not correct";
      valid = false;
    }
    this.setState({errors: errors});
    return valid;
  }

  getData() {
    return {
      email: this.state.email,
      password: this.state.password,
      remember: this.state.remember
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    let one = e.target.checkValidity();
    let valid = this.validateFields();
    //valid = this.bankAccounts.validateFields() && valid;  // break shortcircuit and always call `bankAccounts.validateFields`
    if (one && valid) {//&& alert(JSON.stringify(this.getData()));
      fetch('https://reqres.in/api/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.getData())
      }).then((response) => {
        if (response.status == 200) {
          alert("You have successfully logged in. Need to work on next step.");
        }
        else {
          let errors = {};
          errors.response = "Your Username or password is incorrect. Try again.";
          this.setState({errors: errors});
        }
      });
    }
  }

  render() {
    return (
      <div className={['login-page-section', ' gray-bg-light'].join(' ')}>
        <div className={['container-fluid login-order', 'gray-bg3'].join(' ')}>
          <div className="row">
            <div className={['col-sm-6', 'col-md-5', 'col-md-offset-1', 'padd-1', 'gray-bg-light'].join(' ')}>
              <div className="text-center">
                <h5 className="mt30">Login</h5>
              </div>
              <div className="forms-default-settings">
                <div className="consumer-register-form">
                  <form onSubmit={this.handleSubmit}
                        className={['form contact-form', 'modified-form clearfix'].join(' ')} id="login-form"
                        noValidate="novalidate" data-active="true">
                    <div className="form-section">
                      <div className={['form-group', 'floating-label-input'].join(' ')}>
                        <input name="username" id="username" required type="text" onChange={this.handleEmailChange}/>
                        <label>
                          Username*
                        </label>
                        <span className="error">{this.state.errors.email}</span>
                      </div>
                      <div className={['form-group', 'floating-label-input'].join(' ')}>
                        <input name="password" id="password" type="password" required
                               onChange={this.handlePasswordChange}/>
                        <label>
                          Password*
                        </label>
                        <span className="error">{this.state.errors.password}</span>
                      </div>
                      <div className="form-group">
                        <div
                          className={['horizontal-actions', 'horizontal-checkboxes', 'form-section', 'text-left'].join(' ')}>
                          <div className={['radio', 'radio-inline'].join(' ')}>
                            <input type="checkbox" name="setAsPrimary" id="setAsPrimary" checked={this.state.remember}
                                   onChange={this.handleRememberCheckbox}/>
                            <label htmlFor="setAsPrimary">Remember me </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={['col-xs-12', 'text-center'].join(' ')}>
                      <button className={['btn', 'btn-default', 'active', 'ripple-effect'].join(' ')} type="submit"
                              tabIndex="4">
                        Login
                      </button>
                      <span className="error">{this.state.errors.response}</span>
                    </div>
                    <div className={['forget1', 'text-left'].join(' ')}>
                    </div>
                    <div className={['forget1', 'forget1-pass', 'text-right'].join(' ')}>
                      <Link to="/password">Forgot Password</Link>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-md-5 gray-bg-light text-center padd-1 box-border">
              <h5 className="mt30">Don’t have an account yet?</h5>
              <p className="body-standard gray-login mt30 mb30">Register now to have easy access to manage your
                accounts.</p>
              <Link to="/register" className="btn btn-default">Create an account</Link>

              <div className="col-xs-12 text-center forget2 forget2-page col-xs-12">
                <a href="#"> Login with Mobile Connect</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
